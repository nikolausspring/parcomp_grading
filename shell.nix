{ pkgs ? import <nixpkgs> {} }:
let 
  pypkg = pkgs.python3;
  py-with-pkgs = pypkg.withPackages (p: with p; [
    readchar
    colorama
  ]);
in
pkgs.mkShell {
  buildInputs = [
    py-with-pkgs

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
  shellHook = ''
    PYHTHONPATH=${py-with-pkgs}/lib/python${pypkg.pythonVersion}/site-packages
    # possibly other environment variables
  '';
}
